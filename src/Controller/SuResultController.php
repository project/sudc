<?php
/**
 * Contains \Drupal\sudc\Controller\SuResultController.
 *
 * Controller for handling SearchUnify results and related API endpoints.
 *
 * php version 7.2
 * 
 * @category    Drupal
 * @package     Drupal\sudc\Controller
 * @author      Your Name <your.email@example.com>
 * @license     GNU General Public License version 2 or later; see LICENSE
 * @link        http://grazitti.com
 * @php_version 7.2 
 */
 
namespace Drupal\sudc\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\sudc\Services\CommonCalls;
use Drupal\sudc\Services\RestCalls;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

require_once __DIR__ . '/../../vendor/autoload.php';
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

/**
 * Class SuHelpController.Defines controller for sudc search results.
 *
 * @category    Drupal
 * @package     Drupal\sudc\Controller
 * @author      sudc <your.email@example.com>
 * @license     GPL-2.0-or-later <https://www.gnu.org/licenses/gpl-2.0.html>
 * @link        http://graztti.com
 * @php_version 7.2 
 */
class SuResultController extends ControllerBase
{

    /**
     * A variable for service.
     *
     * @var \Drupal\sudc\CommonCalls
     */
    protected $ccall;

    /**
     * A variable for service.
     *
     * @var \Drupal\sudc\RestCalls
     */
    protected $rcall;

    /**
     * Defining cfactory variable for config factory.
     *
     * @var \Drupal\Core\Config\ConfigFactoryInterface
     */
    protected $cfactory;

    /**
     * Class constructor.
     *
     * @param \sudc\CommonCalls              $ccall    CommonCalls service.
     * @param \sudc\RestCalls                $rcall    RestCalls service.
     * @param \Config\ConfigFactoryInterface $cfactory configuration factory service.
     */
    public function __construct(
        CommonCalls $ccall,
        RestCalls $rcall,
        ConfigFactoryInterface $cfactory
    ) {
        $this->ccall    = $ccall;
        $this->rcall    = $rcall;
        $this->cfactory = $cfactory->get('sudc.configs');

    }

    /**
     * {@inheritdoc}
     *
     * A new instance of the class.
     *
     * @param \ContainerInterface $container service container.
     *
     * @return static
     *   A new instance of the class.
     */
    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('sudc.commonCalls'),
            $container->get('sudc.restCalls'),
            $container->get('config.factory')
        );
    }

    /**
     * Method to get result on frontend.
     *
     * @param string $dynamic_path dynamic path.
     *
     * @return array
     *   An array containing theme information and module path.
     */
    public function content($dynamic_path)
    {
        $path = $dynamic_path;
        $repeater = $this->cfactory->get('repeater_data');
        $num_fields = $this->cfactory->get('num_fields');
        $urls = [];
      
        for ($i = 0; $i < $num_fields; $i++) {
            $urls[$repeater[$i]['uid']] = $repeater[$i]['search_url'];
        }
        
        $matchedUid = null;
        foreach ($urls as $uid => $url) {
            if ($path == $url) {
                $matchedUid = $uid;
            }
        }

        $suConfigs           = [];
        $suConfigs['uid']    = $matchedUid;
        $suConfigs['token']  = $this->cfactory->get('access_token');
        $suConfigs['epoint'] = $this->cfactory->get('epoint');
        $suConfigs['cdn']    = $this->cfactory->get('cdn');

        return [
        '#theme' => 'su_results',
        '#data' => $suConfigs,
        ];
    }

    /**
     * Method to get result in API.
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *   The JSON response.
     */
    public function resultsByPost()
    {

        $requestVals = \Drupal::request();
        $reqData     = $requestVals->getContent();
        $reqDataJson = json_decode($reqData);
        $postData    = (array) $reqDataJson;

        $configVals = $this->rcall->getConfigVals();

        if (($configVals['status'] == true)  
            && (isset($configVals['data']))  
            && (!empty($configVals['data']))
        ) {
            $uid             = $configVals['data']['uid'];
            $token           = $configVals['data']['token'];
            $epoint          = $configVals['data']['epoint'];
            $reqUrl          = $epoint . 'search/searchResultByPost';
            $postData['uid'] = $uid;
            $postData['accessToken'] = $token;

            $resAry = $this->rcall->exeHttpClient(
                'POST',
                $reqUrl,
                $postData
            );
            
            if ($resAry['status'] == 200) {
                $reqResponse = json_decode($resAry['body']);
                $reqResponse = (array) $reqResponse;
                return new JsonResponse($reqResponse);
            } else {
                return new JsonResponse(
                    [
                    'status' => $resAry['status'],
                    'message' => $resAry['message']
                    ]
                );
            }
        } else {
            return new JsonResponse(['status' => 400, 'message' => 'failed']);
        }
    }
    /**
     * Method to get params in API.
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *   The JSON response.
     */
    public function getAPIParams()
    {
        $request = \Drupal::request();
        $requestData = $request->getContent();
        $header = getallheaders();
        $configVals = $this->rcall->getConfigVals();

        if (($configVals['status'] == true)  
            && (isset($configVals['data'])) 
            && (!empty($configVals['data']))
        ) {

            $token           = $configVals['data']['token'];
            $epoint          = $configVals['data']['epoint'];
            $reqUrl          = $epoint . 'mlService/su-gpt';
            $resAry          = $this->rcall->exeHttpClientSuGpt(
                'POST',
                $reqUrl,
                $header,
                $token,
                $requestData
            );

            if ($resAry['status'] == 200) {

                $payload_data = json_decode($requestData);

                if (property_exists($payload_data, 'streaming') 
                    && $payload_data->streaming
                ) {
                    return new JsonResponse($resAry['body']);
                } else {
                    $reqResponse = json_decode($resAry['body']);
                    return new JsonResponse($reqResponse);
                }

            } else {
                return new JsonResponse(
                    [
                    'status' => $resAry['status'],
                     'message' => $resAry['message']
                    ]
                );
            }
        } else {
            return new JsonResponse(['status' => 400, 'message' => 'failed1']);
        }
    }
    /**
     * Method to generate a JWT token for search.
     *
     * This method creates a JWT token based on the user's information and
     * returns the token along with other configuration details.
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *   A JSON response containing the token and user data.
     */
    public function getsearchjwt()
    {

        // Default UID
        $default_uid = '';
        $repeater_data = $this->cfactory->get('repeater_data');
        $num_fields = $this->cfactory->get('num_fields');
        for ($i = 0; $i < $num_fields; $i++) {
            $default_uid = $repeater_data[$i]['uid'];
            break;
        }

        // Get the referrer URL (if available)
        $referrerURL = isset($_SERVER['HTTP_REFERER']) 
        ? $_SERVER['HTTP_REFERER'] 
        : null;    

        // Check if referrer URL exists
        if ($referrerURL) {
            // Parse the URL using parse_url
            $parsedUrl = parse_url($referrerURL);
            $search_url = end(explode("/", $parsedUrl['path']));  
            $search_data = $this->content($search_url);   
            if (!empty($search_data)) {
                $uid_value = $search_data['#data']['uid'] != null
                ? $search_data['#data']['uid']
                : $default_uid;
            }            
        } else {
            $uid_value = $default_uid;
        }

        $user = \Drupal::currentUser();

        // Extract relevant user data.
        $userData = new \stdClass();
        $userData->id = $user->id();
        $userData->email = $user->getEmail();
        $userData->roles = $user->getRoles();


        $configVals = $this->rcall->getConfigVals();

        if (($configVals['status'] == true)  
            && (isset($configVals['data'])) 
            && (!empty($configVals['data']))
        ) {
                
            // Get current timestamp in seconds
            $current_time = time();

            $token_expiry = $configVals['data']['token_expiry'] != ''
             ? $configVals['data']['token_expiry']
            : 180;

            $expiration_time = $current_time + ($token_expiry * 60);

            $access_token = $configVals['data']['token'];

            // Payload data
            $jwt_payload_data = array(
            'access_token' => $access_token,
            'iat' => $current_time,
            'exp' => $expiration_time,
            'email' => $userData->id != 0 ? $userData->email : '',
            'UserId' => $userData->id,
            'ProfileID' => '',
            'UserType' => '',
            'ContactId' => '',
            'AccountID' => '',
            'caseUid' => '',
            'roles' => $userData->id != 0 ? $userData->roles : '',
            );

            $jwt_secret_key = base64_encode($access_token.$userData->id);

            $token = JWT::encode($jwt_payload_data, $jwt_secret_key, 'HS256');

            if (!empty($token)) {
                $response_data = array(
                'token' => $token,
                'user' => $userData,
                'uid' => $uid_value,
                'instanceUrl' => $configVals['data']['epoint'],
                'cdnUrl' => $configVals['data']['cdn'],
                'email' => $userData->id != 0 ? $userData->email : '',
                'success' => true,
                );
      
                return new JsonResponse($response_data);

            } else {
                return new JsonResponse(
                    [
                    'status' => 500,
                     'message' => 'Technical error. Please try after some time'
                    ]
                );
            }

        } else {
            return new JsonResponse(['status' => 400, 'message' => 'failed']);
        }

    }
}
