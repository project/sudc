<?php
/**
 * Class SuHelpController.Defines controller for sudc search results.
 * 
 * Class SuHelpController.Defines controller for sudc search results.
 *  
 * php version 7.2
 * 
 * @category Class
 * @package  Drupal\sudc\Services
 * @author   Your Name <your.email@example.com>
 * @license  GNU General Public License version 2 or later; see LICENSE
 * @link     http://grazitti.com
 * @php      8.3.12 
 */

namespace Drupal\sudc\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class SuHelpController.Defines controller for sudc search results.
 *
 * @category Drupal
 * @package  Drupal\sudc\Controller
 *
 * @author sudc <your.email@example.com>
 *
 * @license GPL-2.0-or-later <https://www.gnu.org/licenses/gpl-2.0.html>
 * @link    http://graztti.com
 */
class SuHelpController extends ControllerBase
{

    /**
     * Provides content for the SuHelpController.
     *
     * @return array
     *   An array containing theme information and module path.
     *
     * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
     * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
     */
    public function content()
    {
        global $base_url;
        $moduleHandler = \Drupal::service('extension.list.module');
        $modulePath = $base_url . '/' . $moduleHandler->getPath('sudc');
        return [
        '#theme' => 'helpcontent',
        '#mpath' => $modulePath,
        ];
    }

}
