<?php

/**
 * This file contains the RestCalls class, which manages HTTP requests
 * and configuration handling for the SearchUnify module.
 * php version 8.3.12
 * 
 * @category Class
 * @package  Drupal\sudc\Services
 * @author   Your Name <your.email@example.com>
 * @license  GNU General Public License version 2 or later; see LICENSE
 * @link     http://grazitti.com
 */

namespace Drupal\sudc\Services;

use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

/**
 * RestCalls Class Doc Comment.
 *
 * Endpoint Helper to retrieve application-wide
 * URLs based on the active web instance.
 *
 * @category Class
 * @package  Drupal\sudc\Services
 * @author   Your Name <your.email@example.com>
 * @license  GNU General Public License version 2 or later; see LICENSE
 * @link     http://grazitti.com
 */
class RestCalls
{
    // Class properties
    protected $provision_key = null;
    protected $uid = null;
    protected $epoint = null;
    protected $accsestoken = '';
    protected $token_expiry = '';
    protected $cdn = '';
    protected $sslVerify = true;
    protected $httpClient;
    protected $cfactory;
    protected $suConfigs;
    
    /**
     * The constructor.
     *
     * @param \GuzzleHttp\Client             $httpClient HTTP client manager service
     * @param \config\ConfigFactoryInterface $cfactory   Config factory service
     *
     * @throws \Exception
     *   If the request content is invalid.
     */
    public function __construct(Client $httpClient, ConfigFactoryInterface $cfactory)
    {
        $this->httpClient = $httpClient;
        $this->cfactory = $cfactory->get('sudc.configs');

        $request = \Drupal::request();
        $content = $request->getContent();
        $data = json_decode($content, true);

        $this->uid = isset($data['uid']) ? $data['uid'] : null;
        $this->provision_key = $this->cfactory->get('provision_key');
        $this->epoint = $this->cfactory->get('epoint');
        $this->accsestoken = $this->cfactory->get('access_token');
        $this->cdn = $this->cfactory->get('cdn');
        $this->token_expiry = $this->cfactory->get('token_expiry');
        $this->suConfigs = $cfactory->getEditable('sudc.configs');
    }

    /**
     * Method to generate Access Token from SearchUnify Server.
     *
     * @param string $authUrl  The endpoint URL to execute.
     * @param array  $authbody The body content to be passed in the request.
     *
     * @return array
     *   The response array with status and body.
     */
    public function genOauthToken($authUrl, array $authbody)
    {
        $reqParam = [
            'verify' => $this->sslVerify,
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'cache-control' => 'no-cache',
            ],
            'form_params' => $authbody,
        ];

        $resAry = [];
        try {
            $exeReq = $this->httpClient->request('POST', $authUrl, $reqParam);
            $resAry['status'] = $exeReq->getStatusCode();
            $resAry['body'] = $exeReq->getBody()->getContents();
            return $resAry;
        } catch (RequestException $e) {
            $resAry['status'] = 400;
            $resAry['message'] = 'Invalid Request';
            return $resAry;
        }
    }

    /**
     * Method to get Configuration Values.
     *
     * @return array
     *   An array containing configuration values.
     *   - 'status': boolean, the status of the operation.
     *   - 'data': array, configuration data.
     */
    public function getConfigVals()
    {
        // Check if "Access Token Expiry" is not empty.
        if (!empty($this->uid) 
            && !empty($this->accsestoken) 
            && !empty($this->epoint)
        ) {
            return [
                'status' => true,
                'data' => [
                    'uid' => $this->uid,
                    'token' => $this->accsestoken,
                    'epoint' => $this->epoint,
                    'cdn' => $this->cdn,
                    'token_expiry' => $this->token_expiry,
                ],
            ];
        } elseif (!empty($this->accsestoken) && !empty($this->epoint)) {
            return [
                'status' => true,
                'data' => [
                    'token' => $this->accsestoken,
                    'epoint' => $this->epoint,
                    'cdn' => $this->cdn,
                    'token_expiry' => $this->token_expiry,
                ],
            ];
        } else {
            return ['status' => false, 'data' => []];
        }
    }

    /**
     * Method to execute cURL requests.
     *
     * @param string $reqType The HTTP request type (GET, POST, etc.).
     * @param string $reqUrl  The URL to make the HTTP request to.
     * @param array  $reqbody body of the HTTP request.
     *
     * @return array
     *   An array containing the HTTP status code and the response body.
     */
    public function exeHttpClient($reqType, $reqUrl, array $reqbody)
    {
        $reqParam = [
            'verify' => $this->sslVerify,
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'cache-control' => 'no-cache',
            ],
            'form_params' => $reqbody,
        ];

        $resAry = [];
        try {
            $exeReq = $this->httpClient->request($reqType, $reqUrl, $reqParam);
            $resAry['status'] = $exeReq->getStatusCode();
            $resAry['body'] = $exeReq->getBody()->getContents();
            return $resAry;
        } catch (ConnectException $e) {
            $resAry['status'] = 400;
            $resAry['message'] = 'Invalid Request';
            return $resAry;
        } catch (ClientException $e) {
            $resAry['status'] = $e->getResponse()->getStatusCode();
            $resAry['message'] = 'Client Error: ' . 
            $e->getResponse()->getReasonPhrase();
            $resAry['body'] = $e->getResponse()->getBody()->getContents();
            return $resAry;
        } catch (RequestException $e) {
            $resAry['status'] = 500;
            $resAry['message'] = 'Request Exception: ' . $e->getMessage();
            return $resAry;
        } catch (\Exception $e) {
            $resAry['status'] = 500;
            $resAry['message'] = 'Unknown Error: ' . $e->getMessage();
            return $resAry;
        }
    }

    /**
     * Method to execute cURL requests with authorization.
     *
     * @param string $reqType   HTTP request type (GET, POST, etc.).
     * @param string $reqUrl    URL to make the HTTP request to.
     * @param array  $headers   authorization headers to include in the request.
     * @param string $authToken authorization token to include in the request header.
     * @param string $reqbody   body of the HTTP request.
     *
     * @return array
     *   An array containing the HTTP status code and the response body.
     */
    public function exeHttpClientSuGpt(
        $reqType,
        $reqUrl, 
        array $headers, 
        $authToken, 
        $reqbody
    ) {
        $reqParam = [
            'verify' => $this->sslVerify,
            'headers' => [
                'token' => $authToken,
                'Content-Type' => 'application/json',
                'cache-control' => 'no-cache',
            ],
            'body' => $reqbody,
        ];

        $resAry = [];
        try {
            $exeReq = $this->httpClient->request($reqType, $reqUrl, $reqParam);
            $resAry['status'] = $exeReq->getStatusCode();
            $resAry['body'] = $exeReq->getBody()->getContents();
            return $resAry;
        } catch (ConnectException $e) {
            $resAry['status'] = 400;
            $resAry['message'] = 'Invalid Request';
            return $resAry;
        } catch (ClientException $e) {
            $resAry['status'] = $e->getResponse()->getStatusCode();
            $resAry['message'] = 'Client Error: ' . 
            $e->getResponse()->getReasonPhrase();
            $resAry['body'] = $e->getResponse()->getBody()->getContents();
            return $resAry;
        } catch (RequestException $e) {
            $resAry['status'] = 500;
            $resAry['message'] = 'Request Exception: ' . $e->getMessage();
            return $resAry;
        } catch (\Exception $e) {
            $resAry['status'] = 500;
            $resAry['message'] = 'Unknown Error: ' . $e->getMessage();
            return $resAry;
        }
    }
}
