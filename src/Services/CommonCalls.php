<?php

/**
 * This file contains the RestCalls class, which manages HTTP requests
 * php version 8.3.12
 * 
 * @category Class
 * @package  Drupal\sudc\Services
 * @author   Your Name <your.email@example.com>
 * @license  GNU General Public License version 2 or later; see LICENSE 
 * @link     http://grazitti.com
 */

namespace Drupal\sudc\Services;

use Drupal\Core\Render\RendererInterface;

/**
 * Commoncalls Class Doc Comment.
 *
 * Endpoint Helper to retrieve application-wide
 * URLs based on an active web instance.
 *
 * @category Class
 * @package  SU
 * @author   Your Name <your.email@example.com>
 * @license  GNU General Public License version 2 or later; see LICENSE
 * @link     http://graztti.com
 *
 * @since 1.0.1
 */
class CommonCalls
{

    /**
     * Defining rendertemp.
     *
     * @var \Drupal\sudc\commonCalls
     */
    protected $rendertemp;

    /**
     * The constructor.
     *
     * @param \Render\RendererInterface $rendertemp The renderer interface.
     */
    public function __construct(RendererInterface $rendertemp)
    {
        $this->rendertemp = $rendertemp;
    }

    /**
     * Fetch module header.
     *
     * @return string
     *   The rendered module header.
     */
    public function grazittiHeader()
    {
        global $base_url;
        $moduleHandler = \Drupal::service('extension.list.module');
        $modulePath = $base_url . '/' . $moduleHandler->getPath('sudc');
        $resAry = [
        '#theme' => 'sufooter',
        '#mpath' => $modulePath,
        ];
        return $this->rendertemp->render($resAry);
    }

}
