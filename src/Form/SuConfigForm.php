<?php
/**
 * This file defines the configuration form for sudc module.
 * php version 8.3.12
 * 
 * @category Class
 * @package  Drupal\sudc\Services
 * @author   Your Name <your.email@example.com>
 * @license  GNU General Public License version 2 or later; see LICENSE
 * @link     http://grazitti.com
 */
namespace Drupal\sudc\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sudc\Services\CommonCalls;
use Drupal\sudc\Services\RestCalls;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Contains \Drupal\sudc\Form\SuConfigFrom.
 *
 * @category    Drupal
 * @package     Drupal\sudc\Controller
 * @author      sudc <your.email@example.com>
 * @license     GPL-2.0-or-later <https://www.gnu.org/licenses/gpl-2.0.html>
 * @link        http://graztti.com
 * @php_version 7.2
 */
class SuConfigForm extends ConfigFormBase
{

    /**
     * A variable for service.
     *
     * @var \Drupal\sudc\CommonCalls
     */
    protected $ccall;

    /**
     * A variable for service.
     *
     * @var \Drupal\sudc\RestCalls
     */
    protected $rcall;

    /**
     * A Request stack instance.
     *
     * @var Symfony\Component\HttpFoundation\RequestStack
     */
    protected $request;

    /**
     * Constructs a suConfigForm object.
     *
     * @param \RequestStack $request Request stack variable.
     * @param \CommonCalls  $ccall   Common Call Service variable.
     * @param \RestCalls    $rcall   Rest Call Service variable.
     */
    public function __construct(
        RequestStack $request,
        CommonCalls $ccall,
        RestCalls $rcall
    ) {
        $this->request = $request;
        $this->ccall   = $ccall;
        $this->rcall   = $rcall;
    }

    /**
     * {@inheritdoc}
     *
     * @param \ContainerInterface $container container interface object.
     *
     * @return static  A new instance of the class.
     */
    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('request_stack'),
            $container->get('sudc.commonCalls'),
            $container->get('sudc.restCalls')
        );
    }

    /**
     * Set Configuration Id.
     *
     * @return static returns configs
     */
    protected function getEditableConfigNames()
    {
        return [
        'sudc.configs',
        ];
    }

    /**
     * Set Form Id.
     *
     * @return static returns configs
     */
    public function getFormId()
    {
        return 'sudc_configs';
    }

    /**
     * {@inheritdoc}
     *
     * @param array               $form       associative array containing the form.
     * @param \FormStateInterface $form_state current state of the form.
     *
     * @return parentbuildForm
     *   A new instance of the class.
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $suConfigs = $this->config('sudc.configs');
        $devFields = $this->request->getCurrentRequest()->get('df');

        $form['hidden'] = [
        '#type' => 'hidden',
        '#attributes' => ['autocomplete' => 'off'],
        ];

        $form['body'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Configure searchunify access credentials'),
        ];

        $form['body']['cdn'] = [
        '#type' => 'textfield',
        '#title' => $this->t('CDN'),
        '#description' => $this->t('CDN from searchunify app'),
        '#size' => 64,
        '#default_value' => $suConfigs->get('cdn') ?? '',
        '#maxlength' => 200,
        '#required' => true,
        ];

        $form['body']['provision_key'] = [
        '#type' => 'password',
        '#title' => $this->t('Provision Key'),
        '#description' => $this->t('Provision Key from searchunify app '),
        '#size' => 64,
        '#required' => true,
        '#maxlength' => 200,
        '#attributes' => [
        'autocomplete' => 'off',
        'value' => (!empty($suConfigs->get('provision_key')))
         ? $suConfigs->get('provision_key') 
         : '',
        ],
        ];

        $form['body']['epoint'] = [
        '#type' => 'url',
        '#title' => $this->t('Endpoint'),
        '#description' => $this->t(
            'Endpoint URL i.e. https://xxxxxxxx.searchunify.com/'
        ),
        '#size' => 64,
        '#default_value' => $suConfigs->get('epoint') ?? '',
        '#maxlength' => 200,
        '#required' => true,
        ];

        $form['body']['token_expiry'] = [
        '#type' => 'number',
        '#title' => $this->t('Enter expiration time'),
        '#description' => $this->t('Authentication token for search in minutes'),
        '#size' => 64,
        '#default_value' => $suConfigs->get('token_expiry') ?? '180',
        '#maxlength' => 200,
        '#required' => true,
        ];

        $form['#tree'] = true;

        $num_fields = $this->config('sudc.configs')->get('num_fields');
        if ($num_fields == '' || $num_fields == null) {
            $this->config('sudc.configs')->set('num_fields', 1)->save();
        }

        $form['body']['items_fieldset'] = [
        '#type' => 'fieldset',
        '#prefix' => '<div id="items-fieldset-wrapper">',
        '#suffix' => '</div>',
        '#attributes' => [
        'class' => ['repeater-fieldset-wrapper'],
        ],
        ];

        $form['body']['items_fieldset']['actions'] = [
        '#type' => 'actions',
        '#attributes' => [
        'class' => ['repeater-fieldset'],
        ],
        ];
        $form['body']['items_fieldset']['actions']['add_item'] = [
        '#type' => 'submit',
        '#value' => $this->t('<span>&#43;</span> Add'),
        '#submit' => ['::addMore'],
        '#ajax' => [
        'callback' => '::addItemCallback',
        'wrapper' => 'items-fieldset-wrapper',
        ],
        '#attributes' => [
        'class' => ['add-btn'],
        ],
        '#limit_validation_errors' => [],
        ];

        for ($i = 0; $i < $num_fields; $i++) {
            $form['body']['items_fieldset'][$i] = $this->getItemFields(
                $i, $i == 0, $form_state
            );
        }

        if ($devFields == '1') {
            $form['body']['atkn'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Access_Token'),
            '#default_value' => $suConfigs->get('access_token') ?? '',
            '#attributes' => ['disabled' => 'disabled'],
            ];

        }

        $form['body']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Save Configuration'),
        '#prefix' => '<div class="form-action">',
        '#suffix' => '</div>',
        '#attributes' => [
        'class' => ['button', 'button--primary'],
        ],
        ];

        $form['footer'] = [
        '#type' => 'markup',
        '#markup' => $this->ccall->grazittiHeader(),
        ];

        $form['#attributes'] = ['autocomplete' => 'off'];
        $form['#attached']['library'][] = 'sudc/sudc';

        return $form;
    }

    /**
     * Get the form fields for an individual item in the repeater.
     *
     * @param int                 $i           index of the item.
     * @param bool                $isFirstItem indicating this is the first item.
     * @param \FormStateInterface $form_state  current state of the form.
     *
     * @return array
     *   An associative array containing the form fields for the item.
     */
    public function getItemFields($i, $isFirstItem, FormStateInterface &$form_state)
    {
        $repeater = $this->config('sudc.configs')->get('repeater_data');

        if (!$isFirstItem) {
            return [
            'wrapper' => [
            '#type' => 'container',
            '#attributes' => [
            'class' => ['repeater-div'],
            ],
            'uid' => [
            '#type' => 'textfield',
            '#title' => $this->t('UID'),
            '#description' => $this->t('Unique identification key'),
            '#size' => 64,
            '#default_value' => $repeater[$i]['uid'] ?? '',
            '#maxlength' => 200,
            '#required' => true,
            '#attributes' => [
              'autocomplete' => 'off',
              'data-id' => $i,
              'data-uid' => 'default-' . $repeater[$i]['uid'],
            ],
            ],
            'search_url' => [
            '#type' => 'textfield',
            '#title' => $this->t('Search URL'),
            '#description' => $this->t('Search URL (/searchunify/{searchurl})'),
            '#size' => 64,
            '#default_value' => $repeater[$i]['search_url'] ?? '',
            '#maxlength' => 200,
            '#required' => true,
            '#attributes' => [
              'autocomplete' => 'off',
              'data-id' => $i,
            ],
            ],

            'remove_item' => [
            '#type' => 'submit',
            '#value' => $this->t('Remove'),
            '#name' => 'remove_item_' . $i,
            '#attributes' => [
              'class' => ['remove-item-btn'],
              'data-index' => $i,
            ],
            '#submit' => ['::removeOne'],
            '#limit_validation_errors' => [],
            '#ajax' => [
              'callback' => '::removeItemAjaxCallback',
              'wrapper' => 'items-fieldset-wrapper',
            ],
            ],
            ],
            ];
        } else {
            return [
            'wrapper' => [
            '#type' => 'container',
            '#attributes' => [
            'class' => ['repeater-div'],
            ],
            'uid' => [
            '#type' => 'textfield',
            '#title' => $this->t('UID'),
            '#description' => $this->t('Unique identification key'),
            '#size' => 64,
            '#default_value' => $repeater[$i]['uid'] ?? '',
            '#maxlength' => 200,
            '#required' => true,
            '#attributes' => ['autocomplete' => 'off'],
            ],
            'search_url' => [
            '#type' => 'textfield',
            '#title' => $this->t('Search URL'),
            '#description' => $this->t('Search URL (/searchunify/{searchurl})'),
            '#size' => 64,
            '#default_value' => $repeater[$i]['search_url'] ?? '',
            '#maxlength' => 200,
            '#required' => true,
            '#attributes' => ['autocomplete' => 'off'],
            ],
            ],
            ];
        }
    }

    /**
     * Submit handler for adding more items to the repeater.
     *
     * @param array               $form       form structure.
     * @param \FormStateInterface $form_state current state of the form.
     * 
     * @return array
     *   An associative array containing the form fields for the item.
     */
    public function addMore(array &$form, FormStateInterface $form_state)
    {
        $num_fields = \Drupal::config('sudc.configs')->get('num_fields');
        // print_r($num_fields);die();
        \Drupal::configFactory()->getEditable('sudc.configs')
            ->set("num_fields", $num_fields + 1)->save();
        $form_state->setRebuild();
    }

    /**
     * Submit handler for remove one items to the repeater.
     *
     * @param array               $form       form structure.
     * @param \FormStateInterface $form_state current state of the form.
     * 
     * @return array
     *   An associative array containing the form fields for the item.
     */
    public function removeOne(array &$form, FormStateInterface &$form_state)
    {
        $suConfigs = $this->config('sudc.configs');
        $triggering_element = $form_state->getTriggeringElement();
        $num_fields = $suConfigs->get('num_fields');
        $remove_index = str_replace(
            'remove_item_', 
            '', 
            $triggering_element['#name']
        );        
        $repeater_data = $suConfigs->get('repeater_data');

        // Check if the repeater_data for this index exists and remove it.
        if (isset($repeater_data[$remove_index])) {
            unset($repeater_data[$remove_index]);
            $suConfigs->set('repeater_data', array_values($repeater_data))->save();
        }

        // Decrement the num_fields value.
        if ($num_fields > 1) {
            $suConfigs->set("num_fields", $num_fields - 1)->save();
        }

        // Unset the values from form.
        $user_input = $form_state->getUserInput();
        if (isset($user_input['body']['items_fieldset'])) {
            // Remove the value at the specified index and re-index the array.
            array_splice($user_input['body']['items_fieldset'], $remove_index, 1);
            $user_input['body']['items_fieldset'] = array_values(
                $user_input['body']['items_fieldset']
            );            
            // Set the updated values back to the form state.
            $form_state->setUserInput($user_input);
        }

        // Rebuild the form.
        $form_state->setRebuild();
        return true;
    }

    /**
     * Callback for adding more items to the repeater.
     *
     * @param array $form form structure.
     *
     * @return array
     *   The updated form structure.
     */
    public function addItemCallback(array &$form)
    {
        return $form['body']['items_fieldset'];
    }

    /**
     * Callback for removing items from the repeater.
     *
     * @param array $form form structure.
     *
     * @return array
     *   The updated form structure.
     */
    public function removeItemAjaxCallback(array &$form)
    {
        return $form['body']['items_fieldset'];
    }

    /**
     * {@inheritdoc}
     *
     * @param array               $form       form structure.
     * @param \FormStateInterface $form_state current state of the form.
     * 
     * @return array
     *   The updated form structure.
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        parent::submitForm($form, $form_state);
        $configData = [];
        $repeater_data = [];

        $values = $form_state->cleanValues()->getValues();
        if (isset($values['body'])) {
        
            if (substr($values['body']['cdn'], -1) !== '/') {
                 $configData['cdn'] = $values['body']['cdn'].'/';
            } else {
                   $configData['cdn'] = $values['body']['cdn'];
            }
            if (substr($values['body']['epoint'], -1) !== '/') {
                $configData['epoint'] = $values['body']['epoint'].'/';
            } else {
                $configData['epoint'] = $values['body']['epoint'];
            }
        
            //$configData['cdn'] = $values['body']['cdn'];
            $configData['token_expiry'] = $values['body']['token_expiry'];
            $configData['provision_key'] = $values['body']['provision_key'];
            //$configData['epoint'] = $values['body']['epoint'];
            // Corrected key.
            $configData['access_token'] = $values['access_token'];
      
            foreach ($values['body']['items_fieldset'] as $key => $val) {
                if (isset($val['wrapper']['uid']) 
                    && isset($val['wrapper']['search_url'])
                ) {
                    $uid = $val['wrapper']['uid'];
                    $search_url = $val['wrapper']['search_url'];
                    $repeater_data[] = [
                    'uid' => $uid,
                    'search_url' => $search_url,
                    ];
                }
            }

            try {
                foreach ($configData as $key => $val) {
                    $this->config('sudc.configs')->set($key, $val)->save();
                }
                $this->config('sudc.configs')->set(
                    'repeater_data', $repeater_data
                )->save();

                $this->messenger()->addStatus(
                    $this->t('Configurations saved successfully'), 'success'
                );
            }
            catch (Exception $e) {
                $this->messenger()->addError(
                    $this->t('An error occurred while saving the configurations.'),
                    'error'
                );
            }
        }
    }

    /**
     * {@inheritdoc}
     *
     * @param array               $form       form structure.
     * @param \FormStateInterface $form_state current state of the form.
     * 
     * @return array
     *   The updated form structure.
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        $provision_key = $form_state->getValue(['body', 'provision_key']);
        $epoint = $form_state->getValue(['body', 'epoint']);
        $uid_array = $form_state->getValue(['body', 'items_fieldset']);
        
        foreach ($uid_array as $key=>$id) {
            
            if (array_key_exists('wrapper', $id) ) {
                $uid = $id['wrapper']['uid'];

                //check if condition
                if ((!empty($provision_key)) 
                    && (!empty($epoint))   
                    && (!empty($uid)) 
                ) {
                    $authUrl  = $epoint . '/getAutoProvisionToken';
                    $authbody = [
                    'tokenKey'    => $provision_key,
                    'uid'         => $uid,
                    ];
                    try {
                        $tokenRes = $this->rcall->genOauthToken($authUrl, $authbody);
            
                        if (($tokenRes['status'] == '200') 
                            && (isset($tokenRes['body']))  
                            && (!empty($tokenRes['body']))
                        ) {
                            $resData = json_decode($tokenRes['body']);
                            if ($resData->flag == '400' 
                                && $resData->searchToken == "Invalid uid"
                            ) {
                                $form_state->setErrorByName(
                                    'body][items_fieldset]['.$key.'][wrapper][uid', 
                                    $this->t(
                                        'Invalid UID. Please enter correct UID'
                                    )
                                );
                            }
                            if ($resData->flag == '400' 
                                && $resData->searchToken=="Invalid token"
                            ) {
                                $form_state->setErrorByName(
                                    'body][provision_key',
                                    $this->t(
                                        'Invalid Provision Key. Please '
                                        . 'enter correct Provision Key'
                                    )
                                );                                
                            }
                            // Set - Access Token.
                            $aToken = '';
                            if ((isset($resData->searchToken)) 
                                && (!empty($resData->searchToken))
                            ) {
                                $aToken = $resData->searchToken;
                            }
                            $form_state->setValue('access_token', $aToken);
                        } else {
                            $form_state->setErrorByName(
                                'message', 
                                $this->t(
                                    'Invalid configurations to '
                                    .'connect with searchunify.'
                                )
                            );
                              // $form_state->setRebuild();
                        }
                    }
                    catch (Exception $e) {
                        // Echo 'Caught exception: ',  $e->getMessage(), "\n";.
                        $form_state->setErrorByName(
                            'message',
                            $this->t(
                                'Invalid Configurations passed'
                            )
                        );
                    }
                } else {
                    $form_state->setErrorByName(
                        'message',
                        $this->t(
                            'All required fields are mandatory.'
                        )
                    );
                }
            }
        }
    }

}
