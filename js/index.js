
var randomN;
var slide_index = {};
var randomNumber;

function scrollToBottom(element, to, duration)
{
    if (duration < 0) {
        return;
    }
    var scrollTop = element.scrollTop;
    var difference = to - scrollTop;
    var perTick = (difference / duration) * 10;

    setTimeout(
        function () {
            scrollTop = scrollTop + perTick;
            element.scrollTop = scrollTop;
            if (scrollTop === to) {
                return;
            }
            scrollToBottom(element, to, duration - 10);
        }, 10
    );
}
function nextSlide(n, number)
{
    displaySlides((slide_index[number] += n), number);
}

function displaySlides(n, id)
{
    var i;
    var slides = document.getElementsByClassName(id);
    if (slides.length != 0) {
        if (n > slides.length) {
            slide_index[id] = 1;
        }
        if (n < 1) {
            slide_index[id] = slides.length;
        }
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        slides[slide_index[id] - 1].style.display = "block";
    }
}
